from flask import Flask, jsonify, request

app = Flask(__name__)

livros = [
    {
        'id': 1,
        'titulo': 'O diário de um Mago',
        'autor': 'Paulo Coelho'
    },
    {
        'id': 2,
        'titulo': 'Memórias Póstumas de Brás Cubas',
        'autor': 'Machado de Assis'
    },
    {
        'id': 3,
        'titulo': 'O Cortiço',
        'autor': 'Aloísio de Azevedo'
    }
]


@app.route('/livros', methods=['GET'])
def getBooks():
    return jsonify(livros)


@app.route('/livros/<int:id>', methods=['GET'])
def getBookById(id: int):
    for livro in livros:
        if livro.get('id') == id:
            return jsonify(livro)


@app.route('/livros/<int:id>', methods=['PUT'])
def updateById(id: int):
    livro_alterado = request.get_json()
    for indice, livro in enumerate(livros):
        if livro.get('id') == id:
            livros[indice].update(livro_alterado)
    return jsonify(livros[indice])


@app.route('/livros', methods=['POST'])
def insertBook():
    newBook = request.get_json()
    livros.append(newBook)
    return jsonify(livros)


@app.route('/livros/<int:id>', methods=['DELETE'])
def removeBook(id: int):
    for indice, livro in enumerate(livros):
        if livro.get('id') == id:
            del livros[indice]
    return jsonify(livros)


app.run(port=5000, host='localhost', debug=True)
